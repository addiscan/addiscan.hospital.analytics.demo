package com.addiscan.addiscanhospitalanalytics.apiinterface;

import android.accounts.AccountManager;
import android.view.KeyCharacterMap;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Abdu on 4/13/2015.
 */
public class APIHandler {
    private static final String API_END_POINT = "http://192.168.137.1:8092/Analytics";
    private static RestAdapter restAdapter;
    private static AccountManager accountManager;

    static RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {
            request.addHeader(accountManager.getAccountsByType("com.addiscan.authentication").toString(),"Addiscan Analytics App");

        }
    };
    private static RestAdapter getRestAdapter(){
        if(restAdapter==null){
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(API_END_POINT)
                            //.setErrorHandler(new RetrofitErrorHandler())
                            //.setRequestInterceptor(requestInterceptor)
                    .build();
        }
        return restAdapter;
    }



    public static IAddiscanInterface getApiInterface(){
        IAddiscanInterface addiscanApiInterface = null;
        try{
            if(restAdapter == null){
                restAdapter = getRestAdapter();
            }
            addiscanApiInterface = restAdapter.create(IAddiscanInterface.class);

        }catch(Exception e){
            e.printStackTrace();
        }
        return addiscanApiInterface;
    }

    private static class RetrofitErrorHandler implements ErrorHandler {
        @Override
        public Throwable handleError(RetrofitError cause) {
            Response response = cause.getResponse();
            if(response!=null && response.getStatus() == 401){
                try{

                }catch(Exception e){
                    return new KeyCharacterMap.UnavailableException("");
                }
            }
            return null;
        }
    }
}
