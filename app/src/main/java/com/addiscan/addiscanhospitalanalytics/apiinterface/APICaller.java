package com.addiscan.addiscanhospitalanalytics.apiinterface;

import com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale.SaleAnalyticsResponse;
import com.addiscan.addiscanhospitalanalytics.models.sendmodel.DateTimeRange;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Abdu on 4/13/2015.
 */
public class APICaller {
    public Observable<Object> GetSales(DateTimeRange dateTimeRange){
        return APIHandler.getApiInterface().GetSales(dateTimeRange)
                .observeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<SaleAnalyticsResponse, Object>() {
                    @Override
                    public Object call(SaleAnalyticsResponse saleAnalyticsResponse) {
                        return saleAnalyticsResponse.getAnalyticsActionable().getReturnedData();
                    }
                });
    }
}
