package com.addiscan.addiscanhospitalanalytics.apiinterface;

import com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale.SaleAnalyticsResponse;
import com.addiscan.addiscanhospitalanalytics.models.sendmodel.DateTimeRange;

import retrofit.http.Body;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by Abdu on 4/13/2015.
 */
public interface IAddiscanInterface {
    @POST("/Sale")
    public Observable<SaleAnalyticsResponse> GetSales(@Body DateTimeRange dateTimeRange);
}
