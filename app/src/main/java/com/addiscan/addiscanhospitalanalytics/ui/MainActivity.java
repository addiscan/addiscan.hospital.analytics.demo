package com.addiscan.addiscanhospitalanalytics.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.addiscan.addiscanhospitalanalytics.ui.adapter.NavigationDrawerAdapter;
import com.addiscan.addiscanhospitalanalytics.ui.fragment.AssetFragment;
import com.addiscan.addiscanhospitalanalytics.ui.fragment.SaleFragment;
import com.addiscan.addiscanhospitalanalytics.ui.model.NavigationDrawerModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    private View mNavigationDrawer;
    private NavigationDrawerAdapter mNavigationDrawerAdapter;
    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(mToolbar);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.navigationDrawerListView);

        mNavigationDrawerAdapter = new NavigationDrawerAdapter(this,
                setNavigationDrawerAdapter());
        mDrawerList.setAdapter(mNavigationDrawerAdapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        setUpNavigationDrawer();

        selectItem(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
            selectItem(pos);
        }
    }

    private void setUpNavigationDrawer(){
        mNavigationDrawer = findViewById(R.id.navigationDrawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int position) {
        Fragment mFragment = null;
        mFragmentManager = getFragmentManager();

        switch (position) {
            case 1:
                mFragment = new SaleFragment();
                break;
            case 2:
                mFragment = new AssetFragment();
                break;
        }

        if (mFragment != null){
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mNavigationDrawer);
        }
    }

    private List<NavigationDrawerModel> setNavigationDrawerAdapter(){
        List<NavigationDrawerModel> models = new ArrayList<>();

        models.add(new NavigationDrawerModel("BROWSE"));

        models.add(new NavigationDrawerModel("SALE",R.drawable.icon_folderalt ));

        models.add(new NavigationDrawerModel("ASSET",R.drawable.icon_cube ));

        models.add(new NavigationDrawerModel( "PATIENT",R.drawable.icon_avatar ));

        models.add(new NavigationDrawerModel("ROOM",R.drawable.icon_folderalt ));

        models.add(new NavigationDrawerModel( "OTHERS" ));

        models.add(new NavigationDrawerModel("SETTING",R.drawable.icon_settingstwo_gearalt ));

        return models;
    }

}
