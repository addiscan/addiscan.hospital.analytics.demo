package com.addiscan.addiscanhospitalanalytics.ui.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.addiscan.addiscanhospitalanalytics.ui.R;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class AssetFragment extends Fragment {


    public AssetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_asset, container, false);
    }


}
