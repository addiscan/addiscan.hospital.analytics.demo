package com.addiscan.addiscanhospitalanalytics.ui.model;

/**
 * Created by Mohammed on 1/30/2015.
 */
public class NavigationDrawerModel {
    public String title;
    public int icon;
    public boolean isHeader;
    public boolean isChecked = false;

    public NavigationDrawerModel(String title, int icon) {
        this.title = title;
        this.icon = icon;
        this.isHeader = false;
    }

    public NavigationDrawerModel(String title) {
        this.title = title;
        this.isHeader = true;
    }

}
