package com.addiscan.addiscanhospitalanalytics.ui.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.addiscan.addiscanhospitalanalytics.ui.R;

/**
 * Created by Mohammed on 4/12/2015.
 */
public class SaleFragment extends Fragment {
    CardView mCardView;

    public SaleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sale, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mCardView = (CardView) getActivity().findViewById(R.id.cardview);
    }

}
