package com.addiscan.addiscanhospitalanalytics.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.addiscan.addiscanhospitalanalytics.ui.R;
import com.addiscan.addiscanhospitalanalytics.ui.model.NavigationDrawerModel;

import java.util.List;

/**
 * Created by Mohammed on 1/30/2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter{
    private Context mcontext;
    private List<NavigationDrawerModel> mList;

    public NavigationDrawerAdapter(Context context, List<NavigationDrawerModel> list) {
        this.mList = list;
        this.mcontext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isHeader ? 0 : 1;
    }

    @Override
    public boolean isEnabled(int position) {
        return !getItem(position).isHeader;
    }

    @Override
    public NavigationDrawerModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NavigationDrawerModel item = mList.get(position);

        ViewHolder holder = null;

        if (convertView == null) {
            holder = new ViewHolder();

            int layout = ((item.isHeader) ? R.layout.navigation_header_title
                    : R.layout.navigation_item);

            convertView = LayoutInflater.from(mcontext).inflate(layout, null);

            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        if (holder.title != null){
            holder.title.setText(item.title);
        }

        if (holder.icon != null) {
            if (item.icon != 0) {
                holder.icon.setVisibility(View.VISIBLE);
                holder.icon.setImageResource(item.icon);
            } else {
                holder.icon.setVisibility(View.GONE);
            }
        }

        if (!item.isHeader) {
            convertView.setBackgroundResource(R.drawable.seletor_item_navigation);
        }

        return convertView;
    }

    class ViewHolder {
        public TextView title;
        public ImageView icon;

        public ViewHolder(){
        }

    }

}

