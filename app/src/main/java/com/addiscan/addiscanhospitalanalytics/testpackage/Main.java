package com.addiscan.addiscanhospitalanalytics.testpackage;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.addiscan.addiscanhospitalanalytics.R;
import com.addiscan.addiscanhospitalanalytics.apiinterface.APICaller;
import com.addiscan.addiscanhospitalanalytics.models.sendmodel.DateTimeRange;

import java.util.Calendar;
import java.util.Date;

import rx.functions.Action1;

/**
 * Created by Abdu on 4/13/2015.
 */
public class Main extends Activity {

    private Button getBtn;
    private TextView viewSale;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_layout);
        getBtn = (Button) findViewById(R.id.getBtn);
        viewSale = (TextView) findViewById(R.id.viewSale);

    }

    public void GetSale(final View view){

        APICaller caller =new APICaller();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = cal.getTime();

        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

        Date lastDateOfPreviousMonth = cal.getTime();

        Date now = new Date();
        DateTimeRange timeRange = new DateTimeRange(now,lastDateOfPreviousMonth,"Day");

        caller.GetSales(timeRange).subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {

            }
        });
    }
}
