package com.addiscan.addiscanhospitalanalytics.models.sendmodel;

import java.util.Date;

/**
 * Created by Abdu on 4/11/2015.
 */
public class DateTimeRange {
    public Date StartTime;
    public Date EndTime;
    public String TimeSegment;

    public DateTimeRange(Date startTime, Date endTime, String timeSegment) {
        StartTime = startTime;
        EndTime = endTime;
        TimeSegment = timeSegment;
    }
}
