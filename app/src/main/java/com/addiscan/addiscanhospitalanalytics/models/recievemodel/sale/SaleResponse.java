package com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale;

import com.addiscan.addiscanhospitalanalytics.models.common.*;

import java.util.List;

/**
 * Created by Abdu on 4/11/2015.
 */
public class SaleResponse {
    public List<Action> Actions;
    public ResponseStatus ResponseStatus;
    public SaleStatistics SaleStatistics;

    public List<Action> getActions() {
        return Actions;
    }

    public void setActions(List<Action> actions) {
        Actions = actions;
    }

    public ResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public SaleStatistics getSaleStatistics() {
        return SaleStatistics;
    }

    public void setSaleStatistics(SaleStatistics saleStatistics) {
        SaleStatistics = saleStatistics;
    }
}
