package com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale;

/**
 * Created by Abdu on 4/11/2015.
 */
public class SaleStatistics {
    public double TotalRevenue;
    public double AverageSale;
    public double MaximumSale;
    public double MinimumSale;
    public int NumberOfSale;

    public double getTotalRevenue() {
        return TotalRevenue;
    }

    public void setTotalRevenue(double totalRevenue) {
        TotalRevenue = totalRevenue;
    }

    public double getAverageSale() {
        return AverageSale;
    }

    public void setAverageSale(double averageSale) {
        AverageSale = averageSale;
    }

    public double getMaximumSale() {
        return MaximumSale;
    }

    public void setMaximumSale(double maximumSale) {
        MaximumSale = maximumSale;
    }

    public double getMinimumSale() {
        return MinimumSale;
    }

    public void setMinimumSale(double minimumSale) {
        MinimumSale = minimumSale;
    }

    public int getNumberOfSale() {
        return NumberOfSale;
    }

    public void setNumberOfSale(int numberOfSale) {
        NumberOfSale = numberOfSale;
    }
}
