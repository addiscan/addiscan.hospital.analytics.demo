package com.addiscan.addiscanhospitalanalytics.models.common;

/**
 * Created by Abdu on 4/11/2015.
 */
public class ORID {
    public String ClusterId;
    public String ClusterPosition;
    public String RID;

    public String getClusterId() {
        return ClusterId;
    }

    public void setClusterId(String clusterId) {
        ClusterId = clusterId;
    }

    public String getClusterPosition() {
        return ClusterPosition;
    }

    public void setClusterPosition(String clusterPosition) {
        ClusterPosition = clusterPosition;
    }

    public String getRID() {
        return RID;
    }

    public void setRID(String RID) {
        this.RID = RID;
    }
}
