package com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale;

import com.addiscan.addiscanhospitalanalytics.models.common.*;

/**
 * Created by Abdu on 4/11/2015.
 */
public class SaleAnalyticsResponse {
    public ResponseStatus ResponseStatus;
    public SalesAnalyticsActionable AnalyticsActionable;

    public ResponseStatus getResponseStatus() {
        return ResponseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        ResponseStatus = responseStatus;
    }

    public SalesAnalyticsActionable getAnalyticsActionable() {
        return AnalyticsActionable;
    }

    public void setAnalyticsActionable(SalesAnalyticsActionable analyticsActionable) {
        AnalyticsActionable = analyticsActionable;
    }
}
