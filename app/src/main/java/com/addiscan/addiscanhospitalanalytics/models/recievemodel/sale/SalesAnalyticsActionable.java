package com.addiscan.addiscanhospitalanalytics.models.recievemodel.sale;

import com.addiscan.addiscanhospitalanalytics.models.common.*;

import java.util.List;

/**
 * Created by Abdu on 4/11/2015.
 */
public class SalesAnalyticsActionable {
    public List<Action> Actions;
    public Object ReturnedData;

    public List<Action> getActions() {
        return Actions;
    }

    public void setActions(List<Action> actions) {
        Actions = actions;
    }

    public Object getReturnedData() {
        return ReturnedData;
    }

    public void setReturnedData(Object returnedData) {
        ReturnedData = returnedData;
    }
}
